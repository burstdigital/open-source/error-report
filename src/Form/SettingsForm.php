<?php

namespace Drupal\error_report\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Error Report settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'error_report_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['error_report.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $error_report_settings = $this->config('error_report.settings');
    $ignored_channels = $error_report_settings->get('ignored_channels');
    $ignored_domains = $error_report_settings->get('ignored_domains');
    $slack_token = getenv('slack_token');

    $form['slack_url'] = [
        '#type' => 'url',
        '#title' => $this->t('Slack URL'),
        '#default_value' => $error_report_settings->get('slack_url'),
        '#required' => TRUE,
        '#description' => $this->t('Example: https://something.slack.com')
      ];
    $form['slack_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Slack Token'),
        '#default_value' => $slack_token ? $this->t('Defined as ENV variable - $slack_token.') : $error_report_settings->get('slack_token'),
        '#disabled' => $slack_token ? true : false,
        '#description' => $this->t('Slack token should be defined as ENV variable - $slack_token.'),
        '#required' => TRUE
    ];
    $form['slack_channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slack Channel'),
      '#default_value' => $error_report_settings->get('slack_channel'),
      '#required' => TRUE
    ];
    $form['production_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Production mode'),
      '#default_value' => $error_report_settings->get('production_mode') !== NULL ? $error_report_settings->get('production_mode') : TRUE,
      '#required' => FALSE,
      '#description' => $this->t('This will ignore domains below.')
    ];
    $form['ignored_domains'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Domains to be ignored when in Production mode'),
      '#description' => $this->t('One domain per line.'),
      '#default_value' => is_array($ignored_domains) ? implode(PHP_EOL, $ignored_domains) : ".localhost\n.platformsh.site\n.platform.sh",
      '#size' => 25,
    ];
    $form['slack_here'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Should Slack messages include "@here"?'),
      '#default_value' => $error_report_settings->get('slack_here'),
      '#required' => FALSE
    ];
    $form['ignored_channels'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Channel (usually module) names to ignore'),
      '#description' => $this->t('One name per line.'),
      '#default_value' => is_array($ignored_channels) ? implode(PHP_EOL, $ignored_channels) : "search_api\nsearch_api_sol",
      '#size' => 25,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strpos($form_state->getValue('ignored_channels'), ',') !== FALSE) {
      $form_state->setErrorByName('ignored_channels', $this->t('This is separated by line not comma.'));
    }

    if (strpos($form_state->getValue('ignored_domains'), ',') !== FALSE) {
      $form_state->setErrorByName('ignored_domains', $this->t('This is separated by line not comma.'));
    }

    if (substr($form_state->getValue('slack_url'), -1) === '/') {
        $form_state->setErrorByName('slack_url', $this->t('No trailing slash allowed.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Normalize the text to Unix line endings.
    $ignored_channels = str_replace("\r", "\n", $form_state->getValue('ignored_channels'));
    $ignored_channels_array = array_values(
      array_filter(
        array_map('trim', explode("\n", $ignored_channels))
      )
    );

    $ignored_domains = str_replace("\r", "\n", $form_state->getValue('ignored_domains'));
    $ignored_domains_array = array_values(
      array_filter(
        array_map('trim', explode("\n", $ignored_domains))
      )
    );

    $this->config('error_report.settings')
      ->set('slack_url', $form_state->getValue('slack_url'))
      ->set('slack_token', $form_state->getValue('slack_token'))
      ->set('slack_channel', $form_state->getValue('slack_channel'))
      ->set('production_mode', $form_state->getValue('production_mode'))
      ->set('ignored_domains', $ignored_domains_array)
      ->set('slack_here', $form_state->getValue('slack_here'))
      ->set('ignored_channels', $ignored_channels_array)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
