<?php

namespace Drupal\error_report\Logger;

use Drupal\Core\Link;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\Html;

class ErrorReportLogger implements LoggerInterface
{
  use RfcLoggerTrait;
  use StringTranslationTrait;

  private $database;

  /**
   * Constructs a DbLogController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection.
   **/
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = array()) {

    // Report errors only.
    if (!in_array($level, [RfcLogLevel::ERROR, RfcLogLevel::EMERGENCY, RfcLogLevel::CRITICAL])) {
      return;
    }

    // Don't report ignored channels (modules).
    $config = \Drupal::configFactory()->getEditable('error_report.settings');
    $ignoredChannels = $config->get('ignored_channels');

    if (
      is_array($ignoredChannels)
      && in_array(strtolower($context['channel']), array_map('strtolower', $ignoredChannels))
    ) {
      return;
    }

    // Detect localhost and platform envs.
    if ($config->get('production_mode')) {
      $domain = !empty(\Drupal::request()->getHost()) ? \Drupal::request()->getHost() : \Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE]);
      $ignoredDomains = $config->get('ignored_domains');

      foreach ($ignoredDomains as $ignoredDomain) {
        if (strpos($domain, $ignoredDomain) !== FALSE) {
          return;
        }
      }
    }

    // Compare last log msg for that lvl and channel and prevent spamming.
    // Don't report same msgs in a row for the same type and level.
    $msgs = $this->getLastLogMsg($level, $context['channel']);
    if (empty($msgs[0]) && empty($msgs[1])) {
      $message = $message;
    } else {
      if ($msgs[0] === $msgs[1]) {
        return;
      }

      $message = $msgs[0];
    }

    // Get name of a level.
    $levels = [
      RfcLogLevel::ERROR => 'ERROR',
      RfcLogLevel::EMERGENCY => 'EMERGENCY',
      RfcLogLevel::CRITICAL => 'CRITICAL',
    ];
    $logLevel = $levels[$level];
    $sapi = php_sapi_name();

    // Set message and send.
    $referenceUrl = !empty($context['request_uri']) ? $context['request_uri'] : \Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $message = <<<MSG
    *Reference:* $referenceUrl
    *Module:* {$context['channel']}
    *Level:* $logLevel
    *Sapi:* $sapi
    *Msg:* $message
    MSG;

    $this->sendSlackNotification($message);
  }

  /**
   * Sends Slack notification.
   *
   * @param string $message
   * @return bool
   */
  private function sendSlackNotification(string $message) {
    $config = \Drupal::configFactory()->getEditable('error_report.settings');
    $token = !empty(getenv('slack_token')) ? getenv('slack_token') : $config->get('slack_token');

    if (
      empty($uri = $config->get('slack_url'))
      || empty($token)
      || empty($channel = $config->get('slack_channel'))
    ) {
      \Drupal::logger('error_report')
        ->info('Missing configuration for Slack notification. Please fill ' . Link::createFromRoute('here', 'error_report.settings_form')->toString());

      return false;
    }

    // Add @here to a Slack message.
    if ($config->get('slack_here')) {
      $message = "$message \n <!here>";
    }

    $response = \Drupal::httpClient()->post("$uri/api/chat.postMessage", [
      'query' => [
        'token' => $token,
        'channel' => $channel,
        'text' => $message
      ],
      'headers' => [
        'content-type' => "application/json; charset=utf-8",
      ],
      'http_errors' => FALSE,
    ]);

    if ($response->getStatusCode() !== 200) {
      \Drupal::logger('error_report')->error('There was error on request. HTTP code: ' . $response->getStatusCode());

      return false;
    }

    $responseBody = json_decode($response->getBody()->getContents(), TRUE);
    if (!empty($responseBody['error'])) {
      \Drupal::logger('error_report')->error('There was an error in Slack API: ' . $responseBody['error']);

      return false;
    }

    return true;
  }

  /**
   * Gets last two log msgs for that severity (level) and channel.
   *
   * @param integer $severity
   * @param string $channel
   * @return array|false
   */
  private function getLastLogMsg(int $severity, string $channel) {
    $query = $this->database->select('watchdog', 'w');
    $query->fields('w', [
      'wid',
      'uid',
      'severity',
      'type',
      'timestamp',
      'message',
      'variables',
      'link',
    ]);

    $query->where('severity = :severity AND type = :type', [':severity' => $severity, ':type' => $channel]);
    $query->orderBy('timestamp', 'DESC');
    $query->range(0, 2);

    $result = $query->execute();
    $msgs = [];

    foreach ($result as $row) {
      $msgs[] = $this->formatMessage($row);
    }

    return $msgs;
  }

  /**
   * Formats a database log message.
   *
   * @param object $row
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup|false
   *   The formatted log message or FALSE if the message or variables properties
   *   are not set.
   */
  private function formatMessage($row) {
    // Check for required properties.
    if (!isset($row->message, $row->variables)) {
      return false;
    }

    $variables = @unserialize($row->variables);
    // Messages without variables or user specified text.
    if ($variables === NULL) {
      $message = Xss::filterAdmin($row->message);
    } elseif (!is_array($variables)) {
      $message = $this->t('Log data is corrupted and cannot be unserialized: @message', ['@message' => Xss::filterAdmin($row->message)]);
    }
    // Message to translate with injected variables.
    else {
      // Ensure backtrace strings are properly formatted.
      if (isset($variables['@backtrace_string'])) {
        $variables['@backtrace_string'] = new FormattableMarkup(
          '<pre class="backtrace">@backtrace_string</pre>',
          $variables
        );
      }
      $message = $this->t(Xss::filterAdmin($row->message), $variables);
    }

    // Remove all HTML tags.
    return Html::decodeEntities(strip_tags($message));
  }
}
